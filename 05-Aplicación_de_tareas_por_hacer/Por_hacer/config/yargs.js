/// Antiguas optimizaciones ///
const opts_crear = {
    descripcion: {
        alias: 'd',
        demand: true,
        desc: 'Descripción de la tarea por hacer'
    }
};

const opts_actualizar = {
    descripcion: {
        alias: 'd',
    },
    completado: {
        alias: 'c',
        default: true,
        desc: 'Marca como completado o pendiente la tarea'
    }
}

const opts_borrar = {
    descripcion: {
        demand: true,
        alias: 'd',
        desc: 'Descripción de la tarea por borrar'
    }
}

/// Nuevas optimizaciones ///
const descripcion = {
    alias: 'd',
    demand: true,
    desc: 'Descripción de la tarea por hacer'
};

const completado = {
    alias: 'c',
    default: true,
    desc: 'Marca como completado o pendiente la tarea'
};


const argv = require('yargs')
            .command('crear', 'Crear un elemento por hacer', {
                descripcion
            })
            .command('actualizar', 'Atualiza el estado completado de una tarea', {
                descripcion,
                completado
            })
            .command('borrar', 'Borra una tarea', {
                descripcion
            })
            .help()
            .argv;

module.exports = {
    argv
};