const fs = require('fs');

let listadoPorHacer = [];

const crear = (descripcion) => {
    cargarDB();

    let porHacer = {
        descripcion,
        completado: false
    };

    listadoPorHacer.push(porHacer);
    guardarDB();

    return porHacer;
}

const guardarDB = () => {
    let data = JSON.stringify(listadoPorHacer);

    fs.writeFile('db/data.json', data, (err) => {
        if (err) throw new Error('No se pudo grabar', err)
    });
}

const cargarDB = () => {
    try {
        listadoPorHacer = require('../db/data.json');
    } catch (error) {
        listadoPorHacer = [];
    }
}

const getListado = () => {
    cargarDB();
    return listadoPorHacer;
}

const actualizar = (descripcion, completado=true) => {
    cargarDB();

    // NOTA: findIndex nos regresa la posición donde coincide la tarea a buscar.
    //       y -1 si no encontró coincidencias
    // versión original:
    // let index = listadoPorHacer.findIndex( tarea => {
    //     return tarea.descripcion === descripcion;
    // });
    // versión simplificada:
    let index = listadoPorHacer.findIndex( tarea => tarea.descripcion === descripcion );

    if( index>=0 ){
        listadoPorHacer[index].completado = completado;
        guardarDB();
        return true; // Estamos indicando que la tarea se hizó correctamente.
    } else {
        return false;
    }

}

const borrar = (descripcion) => {
    cargarDB();
    let nuevoListado = listadoPorHacer.filter( tarea => tarea.descripcion !== descripcion);
    
    if( nuevoListado.length == listadoPorHacer.length ){
        return false;
    } else {
        listadoPorHacer = nuevoListado;
        guardarDB();
        return true;
    }
}

module.exports = {
    crear,
    getListado,
    actualizar,
    borrar
}