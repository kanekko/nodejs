///////////////2020///////////////
/*
function saludar(nombre){
  let mensaje = `Hola ${nombre}`;
  return mensaje;
}

let saludo = saludar('Canek');

console.log(saludo);
*/

///////////////2021///////////////

const saludar = (nombre) => {
  return `Saludos ${nombre}`; // 'Saludos ' + nombre;
}

console.log( saludar('Canek') );
