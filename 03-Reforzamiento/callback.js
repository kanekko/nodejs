// Los callbacks son funciones que se ejecutan cuando algo sucede
// por ejemplo:
setTimeout(function(){
    console.log('Hola callback');
}, 3000);

// traducción a función de flecha:
setTimeout(()=>{
    console.log('Hola callback de flecha');
},3000);


// Ej2:
let getUsuarioById = (id, callback)=>{
    let usuario = {
        nombre: 'Canek',
        // id: id // así se especificaba anteriormente
        id // en ECMA6 ya no es necesiario, porque se llaman igual
    }
    // suponiendo que no existe un usario con id=20 en la base de datos:
    if(id===20){
        callback(`El usuario con id ${id}, no existe en la base de datos.`);
    } else {
        callback(null, usuario);
    }
}

// es común que el primer parametro de un callback es un error
getUsuarioById(1, (err, usuario)=>{
    if(err){
        return console.log(err);
    }
    console.log('Usuario de base de datos', usuario);
});