/**
 * Función asincrona
 * async
 */
let getNombre = async() => { // El prefijo async denota una función asincrona que regresa una promesa.
    
    // undefined.nombre; // Ejemplo de fallo para ser capturado en el catch de la promesa.
    // throw new Error('No existe un nombre para ese usuario.'); // Disparar errores con mensajes personalizados.

    return 'José';
};
console.log(getNombre());

/*
 * Con la palabra 'async' el código anterior es
 * equivalente al código siguiente:
 * 
let getNombre = () => {
    return new Promise( (resolve, reject) => {
        resolve('Canek');
    });
};*/
getNombre().then( nombre => {
    console.log(nombre);
}).catch( err => {
    console.log('Error de ASYNC', err); // Imprimiendo el detalle del error.
    // console.log('Error de ASYNC'); // Mensaje genérico de error.
});



/**
 * Función de espera
 * await
 */
let getNombre2 = () => {
    return new Promise( (resolve, reject) => {
        setTimeout( () => {
            resolve('Canek');
        }, 3000 );
    });
};

let saludo = async () => {
    let nombre = await getNombre2(); // La funciones await deben estar dentro de una función async.
    return `Hola ${nombre}`;         // Da una "sensación" de funcionamiento sincrono
}

saludo().then( mensaje => {
    console.log(mensaje);
});