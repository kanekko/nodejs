///////////////2020///////////////
/*
//// var ////
var nombre = 'Wolverine';

if (true) {
    var nombre = 'Magneto';
}

var nombre = 'Wolverine1';
var nombre = 'Wolverine2';
var nombre = 'Wolverine3';
var nombre = 'Wolverine4';

console.log(nombre);


//// let ////
let nombre2 = 'Wolverine';

if (true) {
    // let nombre2 = 'Magneto';
    nombre2 = 'Magneto';
}

// nombre2 = 'Wolverine1';
// nombre2 = 'Wolverine2';
// nombre2 = 'Wolverine3';
// nombre2 = 'Wolverine4';

console.log(nombre2);


//// ciclos-var ////
for (var i=0; i<=5; i++) {
    console.log(`i: ${i}`);
}
console.log(i);


//// ciclos-let ////
let j='Hola Mundo';
for (let j=0; j<=5; j++) {
    console.log(`j: ${j}`);
}
console.log(j);
*/

///////////////2021///////////////

//// var ////
var nombre = 'Wolverine';

if (true) {
    var nombre = 'Magneto';
}

console.log(nombre);


//// let ////
let nombre2 = 'Wolverine';

if (true) {
    // let nombre2 = 'Magneto';
    nombre2 = 'Magneto';
    
    console.log(nombre2);
}

console.log(nombre2);


//// const ////
const nombre3 = 'Wolverine';

if (true) {
    const nombre3 = 'Magneto';
    
    console.log(nombre3);
}

console.log(nombre3);


