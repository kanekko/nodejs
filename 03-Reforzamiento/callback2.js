// Simulación Base de Datos
let empleados = [{
                    id: 1,
                    nombre: 'José'
                }, {
                    id: 2,
                    nombre: 'Canek'
                },
                {
                    id: 3,
                    nombre: 'Juan'
                }];

let salarios = [{
                    id: 1,
                    salario: 1000
                },{
                    id: 2,
                    salario: 2000
                }];

// Función para obtener un empleado por ID
let getEmpleado = (id, callback) => {
    // Original:
    // let empleadoDB = empleados.find( empleado => {
    //     return empleado.id === id;
    // }); 

    // Función de flecha reducida:
    let empleadoDB = empleados.find( empleado => empleado.id === id );

    if(!empleadoDB){
        callback(`No existe un empleado con el ID: ${id}`);
    } else {
        callback(null, empleadoDB);
        // callback(null, empleadoDB); // Debemos de estar seguros de mandar a llamar el callback una sola vez.
    }
};

// Función para obtener el salario de un empleado
let getSalario = (empleado, callback) => {
    let salarioDB = salarios.find( salario => salario.id === empleado.id );
    
    if (!salarioDB) {
        callback(`No se encontró un salario para el usuario: ${empleado.nombre}`);
    } else {
        callback(null, {nombre: empleado.nombre,
                        salario: salarioDB.salario,
                        id: empleado.id});
    }
};

// Llamando a la función getEmpleado()
getEmpleado(1, (err, empleado) => {
    if (err) {
        return console.log(err);
    }
    // console.log(empleado);
    getSalario(empleado, (err, salario) => { // 'salario' es la respueta a ejecutar getSalario()
        if (err) {
            return console.log(err);
        } else {
            console.log(`El salario de ${salario.nombre} es de $${salario.salario}`);
        }
    });
});
