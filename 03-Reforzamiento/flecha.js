// Función normal:
function sumar(a, b){
    return a + b;
}
console.log( `Función normal: ${sumar(10, 20)}`);


// Función de flecha:
let sumar_flecha = (a, b) => {
    return a + b;
}
console.log( `Función de flecha: ${sumar_flecha(10, 20)}` );


// Cuando el cuerpo de la función se expresa en una sola línea
// podemos utilizar la siguiente sintaxis:
let sumar_flecha_2 = (a, b) => a + b;
console.log( `Función de flecha reducida: ${sumar_flecha_2(10, 20)}` );



// Ejercicio:
// Función original:
function saludar(){
    return 'Hola mundo.';
}
console.log(saludar());

// Traducción a función de flecha:
let saludar_flecha = () => 'Hola Mundo.';
console.log(saludar_flecha());


// Funciones de flecha con un argumento:
// Original:
function saludar_argument(nombre){
    return `Hola ${nombre}`;
}
console.log(saludar_argument('José'));
// Traducción:
// A:
// let saludar_flecha_translate = nombre => {
//     return `Hola ${nombre}`;
// }
// B:
let saludar_flecha_translate = nombre => `Hola ${nombre}`;
console.log(saludar_flecha_translate('Canek'));


// 1. Función original
let deadpool = {
    nombre: 'Wade',
    apellido: 'Winston',
    poder: 'Regeneración',
    getNombre: function(){
        return `${this.nombre} ${this.apellido} - poder: ${this.poder}`
    }
};
console.log(deadpool.getNombre());

// 2. Función de flecha
let deadpool2 = {
    nombre: 'Wade',
    apellido: 'Winston',
    poder: 'Regeneración',
    getNombre: () => {
        return `${this.nombre} ${this.apellido} - poder: ${this.poder}`
    }
};
console.log(deadpool2.getNombre());

// 3. Cuando quieres hacer referencia a 'this' con funciones de flecha hacemos referencia 
//    a 'global' y no a 'window' como en el navegador, y para estos casos es mejor
//    utilizar la siguiente sintaxis:
let deadpool3 = {
    nombre: 'Wade',
    apellido: 'Winston',
    poder: 'Regeneración',
    getNombre() {
        return `${this.nombre} ${this.apellido} - poder: ${this.poder}`
    }
};
console.log(deadpool3.getNombre());
