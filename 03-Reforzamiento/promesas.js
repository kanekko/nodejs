// Simulación Base de Datos
let empleados = [{
                    id: 1,
                    nombre: 'José'
                }, {
                    id: 2,
                    nombre: 'Canek'
                },
                {
                    id: 3,
                    nombre: 'Juan'
                }];

let salarios = [{
                    id: 1,
                    salario: 1000
                },{
                    id: 2,
                    salario: 2000
                }];

// Función para obtener un empleado por ID
let getEmpleado = (id) => {
    return new Promise( (resolve, reject) => {
        let empleadoDB = empleados.find( empleado => empleado.id === id );

        if(!empleadoDB){
            reject(`No existe un empleado con el ID: ${id}`);
        } else {
            resolve(empleadoDB);
        }
    } );
};

let getSalario = (empleado) => {
    return new Promise( (resolve, reject) => {
        let salarioDB = salarios.find( salario => salario.id === empleado.id );
        
        if (!salarioDB) {
            reject(`No se encontró un salario para el usuario: ${empleado.nombre}`);
        } else {
            resolve({nombre: empleado.nombre,    // resolve se ejecuta una sola vez,
                     salario: salarioDB.salario, // aunque se declare más de una vez.
                     id: empleado.id});
        }        
    } );
};

// Llamando a la promesa
/*getEmpleado(2).then(empleado => {
    // console.log('Empleado de DB', empleado);

    getSalario(empleado).then(salario => {
        console.log(`El salario de ${salario.nombre} es de $${salario.salario}`);
    }, err => console.log(err) );

}, (err) => {
    console.log(err);
});*/

// Promesas en cadena:
getEmpleado(2).then( empleado => {
    return getSalario(empleado);
}).then( salario => {
    console.log(`El salario de ${salario.nombre} es de $${salario.salario}`);
}).catch( err => {
    console.log(err);
});
