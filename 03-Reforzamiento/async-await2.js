let empleados = [{
                    id: 1,
                    nombre: 'José'
                },{
                    id: 2,
                    nombre: 'Canek'
                },{
                    id: 3,
                    nombre: 'Juan'
                }];

let salarios = [{
                    id: 1,
                    salario: 1000
                },{
                    id: 2,
                    salario: 2000
                }];

///
/// Implemetnación de getEmpleado() y getSalario() con async y await
///

let getEmpleado = async (id) => {
    let empleadoDB = empleados.find( empleado => empleado.id === id );

    if(!empleadoDB){
        throw new Error(`No existe un empleado con el ID: ${id}`);
    } else {
        return empleadoDB;
    }
};

let getSalario = async (empleado) => {
    let salarioDB = salarios.find( salario => salario.id === empleado.id );
    
    if (!salarioDB) {
        throw new Error(`No se encontró un salario para el usuario: ${empleado.nombre}`);
    } else {
        return {nombre: empleado.nombre,
                salario: salarioDB.salario,
                id: empleado.id};
    }        
};

// Función que mandará a llamar a getEmpleado() y getSalario()
let getInformacion = async (id) => {
    let empleado = await getEmpleado(id);
    let salario  = await getSalario(empleado);

    return `El salario de ${salario.nombre} es de $${salario.salario}`;
};

getInformacion(2).then( mensaje => console.log(mensaje) )
                 .catch( err => console.log(err) );

