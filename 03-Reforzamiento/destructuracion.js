let deadpool = {
    nombre: 'Wade',
    apellido: 'Winston',
    poder: 'Regeneración',
    getNombre: function(){
        return `${this.nombre} ${this.apellido} - poder: ${this.poder}`
    }
    // getNombre(){ //ECMA6
    //     return `${this.nombre} ${this.apellido} - poder: ${this.poder}`
    // }
};
// console.log(deadpool.getNombre());

//// Sin destructuración ////
// let nombre = deadpool.nombre;
// let apellido = deadpool.apellido;
// let poder = deadpool.poder;

//// Con destructuración ////
//let {nombre, apellido, poder} = deadpool;
//console.log(nombre, apellido, poder);

let {nombre:primerNombre, apellido, poder} = deadpool;
console.log(primerNombre, apellido, poder);