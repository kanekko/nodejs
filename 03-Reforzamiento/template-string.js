////
let nombre      = 'Deadpool';
let nombre_real = 'Wade Winston';

console.log(nombre + ' ' + nombre_real);

// console.log(`${1+2}`);
console.log(`${nombre} ${nombre_real}`);


////
let nombreCompleto = nombre + ' ' + nombre_real;
let nombreTemplate = `${nombre} ${nombre_real}`
console.log(nombreCompleto === nombreTemplate);


////
function getNombre(){
    return `${nombre} es ${nombre_real}`
}
console.log(`El nombre de: ${getNombre()}`);
