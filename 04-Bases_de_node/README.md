## Multiplicar Console App ##

Esta es una aplicación para generar archivos de tablas de multiplicar

Ejecutar este comando para reconstruir los módulos de node:
```
$ npm install
```

Ejecutar este comando para correr la aplicación:
```
$ node app listar --limite 30 -b 3
$ node app crear --limite 5 -b 3
```