// tipos de requireds:
const fs = require('fs'); //paquetes nativos de Node
// const ex   = require('express'); // paquetes No nativos de Node
// const file = require('../file.js'); // archivos locales en el proyecto

const colors = require('colors');

// module.exports.crearArchivo = (base) => { // Otra forma de expotar archivos
let crearArchivo = (base, limite=10) => {
    return new Promise( (resolve, reject) => {

        if (!Number(base)) {
            reject(`El valor introducido ${base} no es un número.`);
            return; // Este return es para detener la ejecición del código
        }

        let data = '';

        // construyendo la tabla de multiplicar
        for (let i=1; i<=limite; i++) {
            // console.log(`${base} * ${i} = ${base*i}`);
            data += `${base} * ${i} = ${base*i}\n`;
        }

        // Escribiendo datos en archivo:
        fs.writeFile(`tablas/tabla-${base}-al-${limite}.txt`, data, (err) => {
            if(err) 
                reject(err)
            else 
                resolve(`tabla-${base}-al-${limite}.txt`);
        });

    });
};

let listarTabla = (base, limite=10) => {

    console.log('--------------------'.blue);
    console.log(`---- Tabla de ${base} ----`.blue);
    console.log('--------------------'.blue);
    

    for (let i=1; i<=limite; i++) {
        console.log(`${base} * ${i} = ${base*i}`);
    }
}

// Formas de agregar objetos de manera global (exportar el archivo):
module.exports = { crearArchivo, listarTabla }