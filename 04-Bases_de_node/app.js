/**
 * Ejecución:
 * $ node app --base=10
 *
 * $ node app listar --limite 30 -b 3
 * $ node app crear --limite 5 -b 3
 */

// requiereds
const argv = require('./config/yargs').argv;
const colors = require('colors/safe');

// La manera tradicional:
// const multiplicar = require('./multiplicar/multiplicar');
// multiplicar.crearArchivo();

// Con destructuración:
const { crearArchivo, listarTabla } = require('./multiplicar/multiplicar');
let comando = argv._[0]; // Obteniendo parametros de yargs

// console.log(module); // Modulo es un objeto global que esta disponible 
                        // a lo largo de toda la aplicación, con detalles de esta.

// console.log(process); // Process es un objeto con datos relacionados al SO
// console.log(process.argv); // argv tiene las rutas de node y el archvio actual y también tiene los argumentos que vienen desde línea de comandos

/// Old version ///
// let base = 8;
// let argv = process.argv;
// let parametro = argv[2];
// let base = parametro.split('=')[1];
/// End old version ///

// console.log(argv);
// console.log(argv.base);
// console.log('Limite', argv.limite);

/// New version ///
switch (comando) {
    case 'listar':
        listarTabla(argv.base, argv.limite);
        break;
    case 'crear':
        crearArchivo(argv.base, argv.limite)
            // .then(archivo => console.log(`Archivo creado: ${archivo}`))
            // .then(archivo => console.log('Archivo creado:' + `${archivo}`.blue))
            .then(archivo => console.log('Archivo creado:', colors.blue(archivo)))
            .catch(e => console.log(e));
        break;
    default:
        console.log('Comando no reconocido');
        // break;
}
